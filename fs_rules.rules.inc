<?php

/**
 * Implements hook_rules_data_info().
 */
function fs_rules_rules_data_info() {
  return array(
    'fs_rules_dir' => array(
      'label' => t('Directory'),
      'group' => t('File system'),
      'wrap' => TRUE,
      'property info' => array(
        'uri' => array(
          'label' => t('URI'),
          'type' => 'text',
          'description' => t("A URI of the directory."),
        ),
      ),
    ),
    'fs_rules_file' => array(
      'label' => t('File'),
      'group' => t('File system'),
      'wrap' => TRUE,
      'property info' => array(
        'uri' => array(
          'label' => t('URI'),
          'type' => 'text',
          'description' => t("A URI of the file."),
        ),
      ),
    ),
  );
}

/**
 * Implements hook_rules_action_info().
 */
function fs_rules_rules_action_info() {
  return array(
    'fs_rules_action_open_dir' => array(
      'label' => t('Open directory'),
      'group' => t('File system'),
      'parameter' => array(
        'uri' => array(
          'type' => 'text',
          'label' => t('URI'),
          'description' => t("A URI or path to directory. <br/>For example: <em>public://exported_files</em>."),
        ),
        'base_dir' => array(
          'type' => 'fs_rules_dir',
          'label' => t('Base directory'),
          'optional' => TRUE,
          'description' => t("(Optional) Parent directory."),
        ),
      ),
      'provides' => array(
        'dir' => array(
          'type' => 'fs_rules_dir',
          'label' => t('Directory'),
        ),
      ),
    ),
    'fs_rules_action_create_dir' => array(
      'label' => t('Create directory'),
      'group' => t('File system'),
      'parameter' => array(
        'uri' => array(
          'type' => 'text',
          'label' => t('URI'),
          'description' => t("A URI or path to directory. <br/>For example: <em>public://exported_files</em>."),
        ),
        'base_dir' => array(
          'type' => 'fs_rules_dir',
          'label' => t('Base directory'),
          'optional' => TRUE,
          'description' => t("(Optional) Parent directory."),
        ),
      ),
      'provides' => array(
        'created_dir' => array(
          'type' => 'fs_rules_dir',
          'label' => t('Created directory'),
        ),
      ),
    ),
    'fs_rules_action_delete_dir' => array(
      'label' => t('Delete directory'),
      'group' => t('File system'),
      'parameter' => array(
        'dir' => array(
          'type' => 'fs_rules_dir',
          'label' => t('Directory'),
          'optional' => TRUE,
          'description' => t("Directory to delete."),
        ),
      ),
    ),
    'fs_rules_action_find_dirs' => array(
      'label' => t('Find directories'),
      'group' => t('File system'),
      'parameter' => array(
        'dir' => array(
          'type' => 'fs_rules_dir',
          'label' => t('Directory'),
          'description' => t("Directory to scan for directories."),
        ),
        'mask' => array(
          'type' => 'text',
          'label' => t('Search mask'),
        ),
        'operation' => array(
          'type' => 'text',
          'label' => t('Comparison operation'),
          'options list' => 'rules_data_text_comparison_operation_list',
          'restriction' => 'input',
          'default value' => 'contains',
          'optional' => TRUE,
          'description' => t('See rules/modules/rules.data.inc for the detailed description on using this.'),
        ),
      ),
      'provides' => array(
        'found_dirs' => array(
          'type' => 'list<fs_rules_dir>',
          'label' => t('Found directories'),
        ),
      ),
    ),
    'fs_rules_action_find_files' => array(
      'label' => t('Find files'),
      'group' => t('File system'),
      'parameter' => array(
        'dir' => array(
          'type' => 'fs_rules_dir',
          'label' => t('Directory'),
          'description' => t("Directory to scan for files."),
        ),
        'mask' => array(
          'type' => 'text',
          'label' => t('Search mask'),
        ),
        'operation' => array(
          'type' => 'text',
          'label' => t('Comparison operation'),
          'options list' => 'rules_data_text_comparison_operation_list',
          'restriction' => 'input',
          'default value' => 'contains',
          'optional' => TRUE,
          'description' => t('See rules/modules/rules.data.inc for the detailed description on using this.'),
        ),
      ),
      'provides' => array(
        'found_files' => array(
          'type' => 'list<fs_rules_file>',
          'label' => t('Found files'),
        ),
      ),
    ),
    'fs_rules_action_copy_file' => array(
      'label' => t('Copy file'),
      'group' => t('File system'),
      'named parameter' => TRUE,
      'parameter' => array(
        'file' => array(
          'type' => array('fs_rules_file', 'file', 'field_item_image', 'field_item_file'),
          'label' => t('File'),
          'description' => t("File to copy."),
        ),
        'dir' => array(
          'type' => 'fs_rules_dir',
          'label' => t('Destination'),
          'description' => t("Destination directory."),
        ),
      ),
      'provides' => array(
        'copied_file' => array(
          'type' => 'fs_rules_file',
          'label' => t('Copied file'),
        ),
      ),
    ),
    'fs_rules_action_save_file' => array(
      'label' => t('Save file to the database'),
      'group' => t('File system'),
      //'named parameter' => TRUE,
      'parameter' => array(
        'file' => array(
          'type' => array('fs_rules_file'),
          'label' => t('File'),
          'description' => t("A file to save."),
        ),
      ),
      'provides' => array(
        'saved_file' => array(
          'type' => 'file',
          'label' => t('Saved file'),
          'description' => t("Saved file entity."),
        ),
      ),
    ),
    'fs_rules_action_add_file_to_list' => array(
      'label' => t('Add a file to a list'),
      'group' => t('Data'),
      'named parameter' => TRUE,
      'parameter' => array(
        'list' => array(
          'type' => array('list<field_item_image>', 'list<field_item_file>'),
          'label' => t('List of files'),
          'description' => t("The file list to an file is to be added."),
        ),
        'file' => array(
          'type' => array('file'),
          'label' => t('File'),
          'description' => t("File to add."),
        ),
      ),
    ),
  );
}

function fs_rules_action_open_dir($uri, $base_dir = NULL) {
  $uri = _fs_rules_sanitize_uri($uri);
  if (isset($base_dir['uri'])) {
    $uri = _fs_rules_join_path($base_dir['uri'], $uri);
  }
  if (is_dir($uri)) {
    return array('dir' => array('uri' => $uri));
  }
}

function fs_rules_action_create_dir($uri, $base_dir = NULL) {
  $uri = _fs_rules_sanitize_uri($uri);
  if (isset($base_dir['uri'])) {
    $uri = _fs_rules_join_path($base_dir['uri'], $uri);
  }
  if (!file_exists($uri)) {
    if (!file_prepare_directory($uri, FILE_CREATE_DIRECTORY)) {
      return FALSE;
    }
  }
  return array('created_dir' => array('uri' => $uri));
}

function fs_rules_action_delete_dir($dir) {
  return drupal_rmdir($dir['uri']);
}

function fs_rules_action_find_dirs($dir, $mask, $op = 'contains') {
  return array('found_dirs' => _fs_rules_wrap_entries_list(_fs_rules_scan($dir['uri'], 'dir', $mask, $op)));
}

function fs_rules_action_find_files($dir, $mask, $op = 'contains') {
  return array('found_files' => _fs_rules_wrap_entries_list(_fs_rules_scan($dir['uri'], 'file', $mask, $op)));
}

function fs_rules_action_copy_file($arguments, RulesPlugin $element, $state) {

  $value_info = $element->getArgumentInfo('file');
  switch ($value_info['type']) {
    case 'field_item_image':
    case 'field_item_file':
    case 'fs_rules_file':
      $source_uri = $arguments['file']['uri'];
      break;
    case 'file':
      $source_uri = $arguments['file']->uri;
      break;
    default:
      return;
  }
  if ($result = file_unmanaged_copy($source_uri, $arguments['dir']['uri'])) {
    return array('copied_file' => array('uri' => $result));
  }
}

function fs_rules_action_save_file($file) {
  global $user;

  $new_file = new stdClass();
  $new_file->uid      = $user->uid;
  $new_file->status   = 0;
  $new_file->filename = drupal_basename($file['uri']);
  $new_file->uri      = $file['uri'];
  $new_file->filemime = file_get_mimetype($file['uri']);
  $new_file->filesize = filesize($file['uri']);

  if ($file_entity = file_save($new_file)) {
    return array('saved_file' => $file_entity);
  }
}

function fs_rules_action_add_file_to_list($arguments, RulesPlugin $element, $state) {
  $file = $arguments['file'];
  $list = $arguments['list'];
  switch ($element->getArgumentInfo('list')['type']) {
    case 'list<field_item_image>':
      $list[] = (array) $file;
      break;
    case 'list<field_item_file>':
      $file->display = 1;
      $file->description = '';
      $list[] = (array) $file;
      break;
    defualt:
      return;
  }
  return array('list' => $list);
}

function _fs_rules_scan($uri, $type, $mask, $op) {
  $entries = array();
  if (is_dir($uri)) {
    if ($dh = opendir($uri)) {
      while (($file = readdir($dh)) !== false) {
        if ($file != '.' && $file != '..') {
          $path = _fs_rules_join_path($uri, $file);
          if (filetype($path) == $type) {
            $match = false;
            switch ($op) {
              case 'contains':
                $match = strpos($file, $mask) !== FALSE;
                break;
              case 'starts':
                $match = strpos($file, $mask) === 0;
                break;
              case 'ends':
               $match = strrpos($file, $mask) === (strlen($file) - strlen($mask));
               break;
              case 'regex':
               $match = (bool) preg_match('/'. str_replace('/', '\\/', $mask) .'/', $file);
               break;
            }
            if ($match) {
              $entries[] = $path;
            }
          }
        }
      }
      asort($entries);
      closedir($dh);
    }
  }
  return $entries;
}

function _fs_rules_join_path($p1, $p2) {
  $sep = '/';
  if (substr($p1, -1) == '/') {
    $sep = '';
  }
  return $p1 . $sep . $p2;
}

function _fs_rules_wrap_entries_list($entries, $type) {
  $items = array_map(function($el) {
    return array('uri' => $el);
  }, $entries);
  usort($items, function($a, $b) {
    return strcmp($a['uri'], $b['uri']);
  });
  return $items;
}

function _fs_rules_sanitize_uri($uri) {
  $uri = preg_replace("[^\w\s\d\.\-_~,;:\[\]\(\]]", '.', $uri);
  return $uri;
}
